public class QueueWithoutNitemsApp {
    public static void main(String[] args) {
        QueueWithoutNitems theQueue = new QueueWithoutNitems(7);

        theQueue.insert("ATOM");
        theQueue.insert("SURAPITCH");
        theQueue.insert("KONGTHONG");
        theQueue.insert("PITCH");
        
        if(theQueue.isFull()==true){
            System.out.println("FULL");
        }else{
            System.out.println("Not FULL");
        }

        theQueue.remove();
        theQueue.remove();
        theQueue.remove();

        theQueue.insert("A");
        theQueue.insert("7");
        theQueue.insert("0");
        theQueue.insert("III");
        theQueue.insert("<3");
        int size = theQueue.size();
        System.out.println("Size Now = "+size);

        while(!theQueue.isEmpty()){
            String n = theQueue.remove();
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println();

        if(theQueue.isEmpty()==true){
            System.out.println("EMPTY");
        }else{System.err.println("Not EMPTY");}
        
        size = theQueue.size();
        System.out.println("Size After Remove = "+size);

        //อันนี้ input ตามสบายเลยนะ นี่แก้จากหนังสือ ที่เป็น queue ของ long เป็น queue ของ String แทน
    }
}
